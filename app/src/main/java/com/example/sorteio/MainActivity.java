package com.example.sorteio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class MainActivity extends AppCompatActivity {
    private Button botaoSorteio;
    private TextView resultadoSorteio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoSorteio = findViewById(R.id.btn);
        resultadoSorteio = findViewById(R.id.resultado);

        botaoSorteio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int numero = new Random().nextInt(11);

                Toast.makeText(MainActivity.this, String.valueOf("seu número é : " + numero), Toast.LENGTH_SHORT).show();
//                resultadoSorteio.setText( ""+ numero);
            }
        });



     }
}


